<?php

namespace zp\client;

/**
 * Interface HttpClientInterface
 * @package zp\client
 */
interface HttpClientInterface
{

    /**
     * Совершает запрос к серверу и возвращает декодированный в массив JSON
     *
     * @param string $url
     * @param string $method
     * @param array|null $params
     * @param array|string|null $body
     * @param array|string|null $headers
     * @return mixed
     */
    public function sendRequest($url, $method = 'GET', $params = null, $body = null, $headers = null);
}
