<?php

namespace zp\client;

/**
 * Class CurlHttpClient
 * @package zp\client
 */
class CurlHttpClient implements HttpClientInterface
{
    /**
     * @inheritdoc
     */
    public function sendRequest($url, $method = 'GET', $params = null, $body = null, $headers = [])
    {
        if (empty($url)) {
            return null;
        }

        if (!empty($params)) {
            $url = $url . '?' . http_build_query($params);
        }

        // !!! сделал такой запрос - сервер запятисотил:
        // https://api.zp.ru/v1/vacancies/?average_salary=true&categories_facets=true&geo_id=826&highlight=true&new_search=1&search_type=fullThrottle&state=1vacancies/?sort=date&g
        // eo_id=826&period=today&limit=100

        $curl = curl_init($url);
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 7,
            CURLOPT_MAXREDIRS => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ]);

        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }

        $content = curl_exec($curl);
        $err = curl_errno($curl);
        if ($err !== 0) {
            throw new HttpClientException("Curl exception: ", curl_error($curl));
        }

        $decodedData = json_decode($content, true);

        if (json_last_error() !== 0) {
            throw new HttpClientException("Invalid parse JSON response: " . json_last_error_msg());
        }

        if ($decodedData === null) {
            return null;
        }
        return $decodedData;
    }
}
