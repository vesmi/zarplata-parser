<?php

namespace zp;

/**
 * Class ZarplataReporter
 * @package zp
 */
class ZarplataReporter
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function reportRubricsVacancies()
    {
        $result = [];
        foreach ($this->data as $item) {
            if (empty($item['rubrics'])) {
                break;
            }

            foreach ($item['rubrics'] as $rubric) {
                if (!isset($result[$rubric['title']])) {
                    $result[$rubric['title']] = 1;
                    continue;
                }

                $result[$rubric['title']]++;
            }
        }
        arsort($result, SORT_NUMERIC);
        return $result;
    }

    public function reportWordsVacancies()
    {
        $result = [];

        foreach ($this->data as $item) {
            if (empty($item['header'])) {
                break;
            }

            $words = explode(' ', $item['header']);
            foreach ($words as $word) {
                // чистим слово от ненужных символов
                $word = preg_replace('/[^a-zA-ZА-Яа-я0-9ёЁ\-\s]/iu', '', $word);
                if ($word === '') {
                    continue;
                }
                $word = mb_strtolower($word);

                if (!isset($result[$word])) {
                    $result[$word] = 1;
                    continue;
                }

                $result[$word]++;
            }
        }
        arsort($result, SORT_NUMERIC);
        return $result;
    }
}
