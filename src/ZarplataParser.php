<?php

namespace zp;

use zp\client\HttpClientInterface;

/**
 * Class ZarplataParser
 * @package zp
 */
class ZarplataParser
{
    const PERIOD_TODAY = 'today';
    const PERIOD_THREE = 'three';
    const PERIOD_WEEK = 'week';
    const PERIOD_MONTH = 'month';

    const GEO_NSK = 826;

    const SORT_DATE = 'date';

    /**
     * @var array
     */
    protected $defaultParams = [
        'sort' => self::SORT_DATE,
    ];

    /**
     * @var array Список разрешенных к передаче параметров
     */
    protected static $allowedParams = [
        // TODO задать разрешенные параметры для запроса
    ];

    /**
     * @var string Базовый API url
     */
    protected $baseUrl;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**'
     * ZarplataParser constructor.
     * @param string $baseUrl
     * @param HttpClientInterface $httpClient
     * @param array $defaultParams
     */
    public function __construct($baseUrl, HttpClientInterface $httpClient, $defaultParams = [])
    {
        $this->baseUrl = $baseUrl;
        $this->httpClient = $httpClient;

        if (is_array($defaultParams) && !empty($defaultParams)) {
            $this->defaultParams = $defaultParams;
        }
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return array
     */
    public function getDefaultParams()
    {
        return $this->defaultParams;
    }

    /**
     * @param array $params
     */
    public function setDefaultParams($params = [])
    {
        $this->defaultParams = $params;
    }

    /**
     * Совершает запрос к методу получения вакансий через HttpClient
     *
     * @param array $params
     * @return mixed
     */
    public function getVacancies($params = [])
    {
        return $this->httpClient->sendRequest(
            $this->baseUrl . 'vacancies/',
            'GET',
            $this->prepareRequestParams($params)
        );
    }

    /**
     * Загружает данные по всем вакансиям
     *
     * @param array $params
     * @return array
     */
    public function getAllVacancies($params = [])
    {
        $result = [];

        $iterations = 0;
        $counter = 0;

        while ($data = $this->getVacancies($params)) {
            if (empty($data['vacancies'])) {
                break;
            }

            // TODO лучше хранить в массиве фиксированного размера и элементы в него
            // TODO за время парсинга может добавиться новый элемент, тоже можно учитывать, чтобы не было дублей
            $result = array_merge($result, $data['vacancies']);

            $params['offset'] += $params['limit'];
            $counter += count($data['vacancies']);
            $iterations++;
        }
        
        // TODO тут лучше было бы возвращать тот же FixedArray или любой другой итерируемый объект с коллекцией. И его же прокидывать во все репортеры итд.
        return $result;
    }

    /**
     * Подготавливает данные для параметров запроса
     *
     * @param array $params
     * @return array
     */
    protected function prepareRequestParams($params = [])
    {
        $params = array_merge($this->defaultParams, $params);
        $params = array_filter($params, function ($value) {
            return !empty($value);
        });

        // TODO проверять, что передаются только разрешенные параметры
        // TODO валидировать

        return $params;
    }
}