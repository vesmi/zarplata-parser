<?php

namespace zp\export;

/**
 * Interface ExportInterface
 * @package zp\export
 */
interface ExportInterface
{

    /**
     * @param array $data
     * @return bool
     */
    public function export(array $data);
}
