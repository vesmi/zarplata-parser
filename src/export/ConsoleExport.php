<?php

namespace zp\export;

/**
 * Class ConsoleExport
 * @package zp\export
 */
class ConsoleExport implements ExportInterface
{
    /**
     * @inheritdoc
     */
    public function export(array $data)
    {
        $num = 1;
        foreach ($data as $key => $value) {
            echo sprintf("%d: %s : %s\n", $num, $key, $value);
            $num++;
        }

        return true;
    }
}
