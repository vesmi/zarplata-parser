<?php

namespace zp\export;

/**
 * Class CsvExport
 * @package zp\export
 */
class CsvExport implements ExportInterface
{
    /**
     * @var string
     */
    private $fileName;

    /**
     * CsvExport constructor.
     * @param $fileName
     */
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @inheritdoc
     */
    public function export(array $data)
    {
        if (!($handle = fopen($this->fileName, 'w'))) {
            return false;
        }

        $num = 1;

        foreach ($data as $key => $value) {
            fputcsv($handle, [$num, $key, $value]);
            $num++;
        }

        fclose($handle);

        return true;
    }
}
