Zarplata.Ru Simple Parser
===

Для проверки
---
1. Склонируйте репозиторий
2. Выполните `composer install`
3. Запустите демку:
```
$ php demo.php
```
должно вывести в консоль ТОП рубрик, а в файл `test.csv` ТОП слов в названиях вакансий

Для подключения к проекту
---
Подключите через composer
```
    ...
    "require: {
        "vesmi/zarplata_parser": "*",
        ...
    },
    "repositories": [
        {
          "type": "vcs",
          "url": "https://gitlab.com/vesmi/zarplata-parser.git"
        },
        ...
    ]
```
