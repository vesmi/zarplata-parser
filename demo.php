<?php

use zp\ZarplataParser;
use zp\client\CurlHttpClient;

require(__DIR__ . '/vendor/autoload.php');

//// Раскомментировать, если хочется автозагрузки без composer'а
//function custom_autoloader($class)
//{
//    $class = str_replace('zp', 'src', $class);
//    $filename = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
//    include($filename);
//}
//spl_autoload_register('custom_autoloader');


const BASE_API_HOST = 'https://api.zp.ru/v1/';

// TODO можно использовать клиент с PSR-7 интерфейсами, для универсальности (Guzzle поддерживает)
$client = new CurlHttpClient();

// инициируем парсер
$parser = new ZarplataParser(BASE_API_HOST, $client);

// получаем данные по всем вакансиям
$data = $parser->getAllVacancies([
    'geo_id' => ZarplataParser::GEO_NSK,
    'period' => ZarplataParser::PERIOD_TODAY,
    'limit' => 100,
    'offset' => 0,
    'fields' => 'id,header,rubrics,add_date'
]);

$reporter = new \zp\ZarplataReporter($data);
$report1 = $reporter->reportRubricsVacancies();
$report2 = $reporter->reportWordsVacancies();

$exporter1 = new \zp\export\ConsoleExport();
$exporter1->export($report1);

$exporter2 = new \zp\export\CsvExport('test.csv');
$exporter2->export($report2);